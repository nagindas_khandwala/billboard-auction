﻿<%@ Page Language="C#" AutoEventWireup="true"   CodeFile="loginPage.aspx.cs" Inherits="loginPage" MasterPageFile ="~/MasterPage.master" %>
   <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <form id="form1" runat="server">
       <div>
    <center>
<h1>LOGIN PAGE</h1>
    <h3>If you are new <asp:HyperLink ID="HyperLink1" runat="server" 
            NavigateUrl="~/registorPage.aspx">Register</asp:HyperLink> &nbsp;Here</h3>
     

        <table class="style11">
            <tr>
                <td class="style6">

                    Username</td>
                <td class="style7">
                    <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                </td>
                <td class="style6">
                    <asp:RequiredFieldValidator ID="usernameRFV" runat="server" 
                        ControlToValidate="txtUsername" ErrorMessage="Username is required" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style8">

                    Password</td>
                <td class="style9">
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td class="style8">
                    <asp:RequiredFieldValidator ID="passwordRFV" runat="server" 
                        ControlToValidate="txtPassword" ErrorMessage="Password is required" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>

                    <asp:Button ID="btnLogin" runat="server" Height="29px" Text="Login" 
                        Width="65px" onclick="btnLogin_Click" />
                </td>
                <td class="style5">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnReset" runat="server" Height="28px" Text="Reset" 
                        Width="59px" onclick="btnReset_Click" />

                </td>
                <td>
                    </td>
            </tr>
        </table>
        
        </center>
        </div>
        </form>
    </asp:Content>
