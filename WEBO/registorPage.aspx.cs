﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class registorPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {

        
        SqlCommand com = null;
        SqlConnection con = null;

        try
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["userDB"].ConnectionString);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();

            }

            String query = "insert into userinfo values (@name,@pass,@role,@email)";
            com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("@name", txtUname .Text );
            com.Parameters.AddWithValue("@pass", txtPwd .Text );
            com.Parameters.AddWithValue("@role", "user");
            com.Parameters.AddWithValue("@email", txtMail .Text);

            com.ExecuteNonQuery();
            /*if (count == 1)
            {
                Session["username"] = txtUsername.Text;
                Response.Redirect("Default.aspx");


            }
            else
            {

                Response.Write("Invalid uername or password");
            }*/
        }
        catch (Exception ex)
        {  

            Response.Write("Error "+ex.Message );
        }
        Response.Redirect("loginPage.aspx");
    }

    
    }
