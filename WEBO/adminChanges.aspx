﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="adminChanges.aspx.cs" Inherits="adminChanges" MasterPageFile ="~/MasterPage.master"  %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <div>
    <asp:GridView ID="gvProduct" runat="server" AutoGenerateColumns="False" DataKeyNames="billboardID" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDeleting="gvProduct_RowDeleting" OnSelectedIndexChanged="gvProduct_SelectedIndexChanged">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField DataField="billboardID" HeaderText="ID" />
                    <asp:BoundField DataField="billName" HeaderText="Name" />
                    <asp:BoundField DataField="billHeight" HeaderText="Height" />
                    <asp:BoundField DataField="billWidth" HeaderText="Width" />
                    <asp:BoundField DataField="billBreadth" HeaderText="Breadth" />
                    <asp:BoundField DataField="billLocation" HeaderText="Location" />
                    <asp:CommandField HeaderText="Edit" SelectText="Edit" ShowSelectButton="True" />
                    <asp:CommandField HeaderText="Delete" SelectText="Delete" ShowDeleteButton="True" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </div>
        <asp:Button Text="Add Product" runat="server" ID="btnAddProduct" 
        OnClick="btnAddProduct_Click"    />
        <div id="divStudentForm" runat="server">
            <table>
            <tr>
                    <td>Billboard Name</td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td>Billboard Height</td>
                    <td>
                        <asp:TextBox ID="txtHeight" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Billboard width</td>
                    <td>
                        <asp:TextBox ID="txtWidth" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>BillboardBreadth</td>
                    <td>
                        <asp:TextBox ID="txtBreadth" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Location</td>
                    <td>
                        <asp:TextBox ID="txtLocation" runat="server" />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                    </td>
                    
                </tr>

                </table>
    </div>
    </form>
    </asp:Content>