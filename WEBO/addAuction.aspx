﻿

<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="addAuction.aspx.cs" Inherits="addAuction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form id="form1" runat="server">

    <div>
        <asp:GridView ID="gvAuction" runat="server" AutoGenerateColumns="False" 
            OnPageIndexChanging="gvAuction_PageIndexChanging" PageSize="2" AllowPaging="True">
                <Columns>
                    <asp:BoundField DataField="billName" HeaderText="Name" />
                    <asp:BoundField DataField="initRate" HeaderText="InitialRate" />
                    <asp:BoundField DataField="startDate" HeaderText="StartDate" />
                    <asp:BoundField DataField="endDate" HeaderText="EndDate" />
                    <asp:TemplateField>
                        <HeaderTemplate>Photo</HeaderTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image1" Height="100" Width="100" runat="server" ImageUrl='<%# "~/images/"+Eval("Image") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        
        
        BillBoardName&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtbillName" runat="server"></asp:TextBox>
        <br />
        <br />
     
        
        Initial BidRate:&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtInitRate" runat="server"></asp:TextBox>
        <br />
        <br />
        Start&nbsp; Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
        <asp:Button ID="btnStartCal" runat="server" onclick="btnStartCal_Click" 
            Text="...." />
        <asp:Calendar ID="Calendar3" runat="server" 
            onselectionchanged="Calendar3_SelectionChanged" Visible="False"></asp:Calendar>
        <br />
        <br />
        End Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
        <asp:Button ID="btnEndCal" runat="server" onclick="btnEndCal_Click" Text=".." />
        <asp:Calendar ID="Calendar4" runat="server" 
            onselectionchanged="Calendar4_SelectionChanged" Visible="False"></asp:Calendar>
        <br />
        <br />
        Select Image&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" onclick="Button1_Click" Text="Button" />
        
    </div>

    </form>

</asp:Content>

