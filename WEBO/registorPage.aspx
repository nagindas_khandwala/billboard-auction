﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="registorPage.aspx.cs" Inherits="registorPage" MasterPageFile ="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <div>
    <h2 align="center">Create A New Account</h2>
    <center>
        <table >
            <tr>
                <td align="right" >
                    UserName</td>
                <td >
                    <asp:TextBox ID="txtUname" runat="server"></asp:TextBox>
                </td>
                <td >
                    <asp:RequiredFieldValidator ID="reqFldValUname" runat="server" 
                        ControlToValidate="txtUname" ErrorMessage="UserName is Required" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right" >
                    Email-id</td>
                <td >
                    <asp:TextBox ID="txtMail" runat="server"></asp:TextBox>
                </td>
                <td >
                    <asp:RequiredFieldValidator ID="reqFldValMail" runat="server" 
                        ControlToValidate="txtMail" ErrorMessage="Email-id is Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    &nbsp;&nbsp;&nbsp;
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="txtMail" ErrorMessage="email id is not correct" 
                        ForeColor="Red" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="right" >
                    Password</td>
                <td >
                    <asp:TextBox ID="txtPwd" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="reqFldValPwd" runat="server" 
                        ControlToValidate="txtPwd" ErrorMessage="Password is Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    Confirm Password</td>
                <td >
                    <asp:TextBox ID="txtConfrmPwd" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td >
                    <asp:RequiredFieldValidator ID="reqFldValConfrmPwd" runat="server" 
                        ControlToValidate="txtConfrmPwd" ErrorMessage="Confirm Password is Required" 
                        ForeColor="Red"></asp:RequiredFieldValidator>
                    <br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToCompare="txtPwd" ErrorMessage="Both must be same" ForeColor="Red" 
                        ControlToValidate="txtConfrmPwd"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td >
                    &nbsp;</td>
                <td >
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td >
                    &nbsp;</td>
                <td >
                    <asp:Button ID="btnRegister" runat="server" Height="32px" 
                        onclick="btnRegister_Click" Text="Register" Width="130px" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        </center>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        <p align="center">&nbsp;</p>
        </div>
    </form>
        </asp:Content>