﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

public partial class addAuction : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["userDB"].ConnectionString);

    SqlCommand cmd = null;
    SqlDataAdapter da = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fillGridView();
        }
    }
    private void fillGridView()
    {

        DataSet ds = new DataSet();
        cmd = new SqlCommand("Select billName,initRate,startDate,endDate ,Image from auction", conn);
        da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        gvAuction.DataSource = ds.Tables[0];
        gvAuction.DataBind();
    }



    protected void gvAuction_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAuction.PageIndex = e.NewPageIndex;
        fillGridView();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string filename = "";
        if (FileUpload1.HasFile)
        {
            try
            {
                filename = Path.GetFileName(FileUpload1.FileName);
                uploadImage(filename);
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
        }
        cmd = new SqlCommand("INSERT INTO  auction (billName, initRate,startDate,endDate ,Image ) VALUES (@Name, @rate,@start ,@end,@Image)", conn);
        cmd.Parameters.AddWithValue("@Name", txtbillName.Text.Trim());
        cmd.Parameters.AddWithValue("@Rate", Convert.ToInt32(txtInitRate.Text.Trim()));
        cmd.Parameters.AddWithValue("@start", Convert.ToDateTime(txtStartDate.Text.Trim()));
        cmd.Parameters.AddWithValue("@end", Convert.ToDateTime(txtEndDate.Text.Trim()));
        cmd.Parameters.AddWithValue("@Image", filename);
        int result = cmd.ExecuteNonQuery();
        conn.Close();
        fillGridView();

    }
    

    private void uploadImage(string filename)
    {
        
        FileUpload1.SaveAs(Server.MapPath("~/images/") + filename);

    }


    protected void btnStartCal_Click(object sender, EventArgs e)
    {
        Calendar3.Visible = true;
    }
    protected void Calendar3_SelectionChanged(object sender, EventArgs e)
    {
        txtStartDate.Text = Calendar3.SelectedDate.ToString();
        Calendar3.Visible = false;
    }
    protected void Calendar4_SelectionChanged(object sender, EventArgs e)
    {
        txtEndDate.Text = Calendar4.SelectedDate.ToString();
        Calendar4.Visible = false;
    }
    protected void btnEndCal_Click(object sender, EventArgs e)
    {
        Calendar4.Visible = true;
    }
}