﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class adminChanges : System.Web.UI.Page
{
    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["userDB"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            divStudentForm.Visible = false;
            loadProducts();
        }
    }
    private void loadProducts()
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string query = "SELECT * FROM billboard;";
            SqlCommand cmd = new SqlCommand(query, conn);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            gvProduct.DataSource = ds.Tables[0];
            gvProduct.DataBind();

            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString ());
        }


    }
    protected void gvProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        divStudentForm.Visible = true;
        string proId = gvProduct.DataKeys[gvProduct.SelectedRow.RowIndex].Value.ToString();
        ViewState["billboardID"] = proId;
        btnSave.Text = "Update";
        getSingleProduct();



    }

    private void getSingleProduct()
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            Int32 proId = Convert.ToInt32(ViewState["billboardID"]);
            string query = "SELECT * FROM billboard WHERE billboardID =@IDS";
            SqlCommand cmd = new SqlCommand(query, conn);

            cmd.Parameters.AddWithValue("@IDS", proId);

            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
               
                txtName.Text =Convert .ToString(dr ["billName"]);
                txtHeight.Text = Convert.ToString(dr["billHeight"]);
                txtWidth.Text = Convert.ToString(dr["billWidth"]);
                txtBreadth.Text = Convert.ToString(dr["billBreadth"]);
                txtLocation.Text = Convert.ToString(dr["billLocation"]);
                
            }
            dr.Close();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }

    }
    protected void gvProduct_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            Int32 prodId = Convert.ToInt32(gvProduct.DataKeys[e.RowIndex].Value);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string query = "DELETE FROM billboard WHERE billboardID =@IDs";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@IDs", prodId);
            cmd.ExecuteNonQuery();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            loadProducts();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            insertProduct();
        }
        else
        {
            updateProduct();
            btnSave.Text = "Save";
        }
        loadProducts();

    }

    private void insertProduct()
    {
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            String query = "insert into billboard values (@name,@height,@width,@breadth, @location)";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@height", Convert .ToDecimal(txtHeight.Text.Trim()));
            cmd.Parameters.AddWithValue("@width", Convert .ToDecimal(txtWidth.Text.Trim()));
            cmd.Parameters.AddWithValue("@breadth", Convert .ToDecimal(txtBreadth.Text.Trim()));
            cmd.Parameters.AddWithValue("@location", txtLocation.Text.Trim());
           
            int i = cmd.ExecuteNonQuery();
            conn.Close();
             
        }
        catch (Exception e)
        {
            Response.Write(e.ToString());
        }

    }

    private void updateProduct()
    {
        try
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
                
            }
            Int32 proId = Convert.ToInt32(ViewState["billboardID"]);
            string query = "UPDATE billboard SET billName = @name, billHeight = @height , billWidth = @width , billBreadth = @breadth , billLocation = @location WHERE billboardID =@ID";
            SqlCommand cmd = new SqlCommand(query, conn);

            cmd.Parameters.AddWithValue("@name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@height", Convert .ToDecimal(txtHeight.Text.Trim()));
            cmd.Parameters.AddWithValue("@width", Convert .ToDecimal(txtWidth.Text.Trim()));
            cmd.Parameters.AddWithValue("@breadth", Convert .ToDecimal(txtBreadth.Text.Trim()));
            cmd.Parameters.AddWithValue("@location", txtLocation.Text.Trim());
            cmd.Parameters.AddWithValue("@ID",proId);
            cmd.ExecuteNonQuery();
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }


        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString ());
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        divStudentForm.Visible = false;
    }
    protected void btnAddProduct_Click(object sender, EventArgs e)
    {
        divStudentForm.Visible = true;
    }
    
}